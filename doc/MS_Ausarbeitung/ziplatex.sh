#! /bin/bash

# -----------------------------------------------------------------------------------------------------
# --- ~/Prog/Shell/Skripte/ziplatex.sh                                                   10.06.2012 ---
# --- ratzfatz (Michael Ehmann)                                                                     ---
# -----------------------------------------------------------------------------------------------------

verz=$(pwd)

for i in *.backup *~ *.aux *.bm *.log *.old *.out *.toc *.zip *.swp *.ps *.dvi *-swp *.swp ; do
  find $(pwd) -name $(echo ${i}) -exec rm {} \; 
done

cd .. 
zip -r $(date +%Y_%m_%d)_${verz##*/}.zip ${verz##*/} 
mv $(date +%Y_%m_%d)_${verz##*/}.zip ${verz##*/}
