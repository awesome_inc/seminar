\documentclass[a4paper,12pt,ngerman]{scrartcl}

% Seitenlayout
\usepackage{geometry}
\geometry{a4paper,tmargin=20mm,bmargin=15mm,lmargin=20mm,rmargin=20mm}

% Zeichensatz
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}

% Formelsatz
\usepackage{amsmath,amssymb}

% Vektorgrafiken malen
\usepackage{tikz}
\usetikzlibrary{backgrounds,calc,through}
\usepackage{color}

\setlength{\parindent}{0pt}

% Sonstiges
\hyphenation{Sch\"a-fer-hund}

\begin{document}

%% 
% Formulierung des Problems in einfachen Worten. Herausstellen der zentralen Elemente.
%
\section*{Problemstellung}

Ziel ist es die Dynamik einer Gruppe von Schafen unter Beisein eines oder mehrerer Schäferhunde zu beschreiben um eine optimale Strategie zur Kontrolle der Schafherde zu entwickeln.
Aus verschiedenen Videoaufnahmen lassen sich einige charakteristische Verhaltensmuster erkennen, welche das Modell erfüllen sollte:
\begin{itemize}
	\item{Einzelne Schafe haben das Bestreben \textit{in der Nähe} der Herde zu bleiben.}
	\item{Wenn in Ruhe gelassen, bleiben die Schafe zwar in einer Gruppe, aber positionieren sich eher verstreut (d.h. sie beanspruchen einen gewissen \textit{Raum zur freien Bewegung}).}
	\item{Die Hunde haben eine \textit{abstoßende Wirkung}, welche ggf. den Gruppenzusammenhalt stören könnte. Um die Herde geschlossen zu halten sind also mehrere Hunde nötig.}
\end{itemize}

%%
% Überführung des Problems in eine mathematische Fragestellung.
%
\section*{Mathematische Formulierung}
Eine Möglichkeit das Problem zu formulieren liegt darin, Schafe und Hunde als eine Gruppe von Partikeln aufzufassen, welche untereinander durch das Wirken von Kräften interagieren.
Schafe und Hunde werden hierbei durch die Bewegung ihres Massenschwerpunkts beschrieben, d.h. durch Kurven im $\mathbb{R}^2$. Da die Hunde (mit gewissen Einschränkungen) direkt gesteuert werden können, werden diese der Einfachheit halber zunächst nicht beachtet.

Genauer: Seien $\mathbf{s}^{(i)} \in C^2(\mathbb{R},\mathbb{R}^2)$ (i = 1,$\dots$,N) die Kurven, welche die Bewegung von N Schafen beschreiben:  
\begin{align*}
	\mathbf{s}^{(i)} &: \mathbb{R}  \rightarrow \mathbb{R}^2 \\
	\mathbf{\ddot{s}}^{(i)} &= \mathbf{f}^{(i)}(s^{(j)}, t) \hspace{1cm} j = 1,\dots,N
\end{align*}
Gesucht ist nun eine Kraft $\mathbf{F} = [\mathbf{f}^{(1)},\ldots,\mathbf{f}^{(N)}]^T$, welche die Gruppe in gewünschter Weise bewegt. Nach dem Superpositionsprinzip lässt sich diese Kraft in anziehende bzw. abstoßende Teilkräfte zerlegen: 
\begin{align*}
	\mathbf{f}^{(i)} &: \mathbb{R} \times \mathbb{R}^{2N} \rightarrow \mathbb{R}^2 \\
	\mathbf{f}^{(i)} &= \sum_{m = 1}^{M_+} \mathbf{f}^{(i)}_{m} + \sum_{m = M_++1}^{M_-} \mathbf{f}^{(i)}_{m} 
\end{align*}
wobei die genaue Anzahl und Beschaffenheit dieser Kräfte noch genauer zu spezifizieren ist.

%%
% Einbinden von physikalischen Gesetzen zur Vervollständigung der Problemstellung
%
\section*{Modellierung der Kräfte}
Ziel ist es nun Kräfte zu finden, welche die Anziehung/Abstoßung der Teilchen untereinander beschreiben. Ein passendes Beispiel aus der Physik liefert die \textit{Coulomb-Kraft}.
Sie beschreibt die Wechselwirkung zweier \textit{kugelsymmetrischer} Teilchen mit kugelsymmetrischer Ladungsverteilung im freien Raum:
\begin{align*}
	\mathbf{F}_{12} &= \frac{q^{(1)}\cdot q^{(2)}}{4\pi\epsilon} \cdot \frac{\mathbf{r}^{(1)} - \mathbf{r}^{(2)}}{|\mathbf{r}^{(1)} - \mathbf{r}^{(2)}| ^3}  \\
	\mathbf{F}_{21} &= - \mathbf{F}_{12}
\end{align*}
\begin{figure}[ht]
\centering
\begin{tikzpicture}[
	position/.style={->, draw=black,fill=black, very thick},
	force/.style={->,>=latex,draw=green,fill=green,very thick,text=green},
	chargep/.style={circle, draw=black, fill=blue,thin},
	chargem/.style={circle, draw=black, fill=red,thin}
	]
      
      \draw (0.5,1) node[chargem]{.};
      \draw (3.5,1.5) node[chargep]{.} ;
      \draw[position] (0,0) -- (0.25,0.5) node[left]{$\mathbf{r}_1$} -- (0.5,1) ;
      \draw[position] (0,0) -- (1.75,0.75) node[below right] {$\mathbf{r}_2$} -- (3.5,1.5) ;
      \draw[position]  (3.5,1.5) -- (2,1.25) node[above left] {$\mathbf{r}$} -- (0.5,1);
      \draw[force] (0.5,1) -- (1,1.08) node[above] {$\mathbf{F}_{12}$} -- (2,1.25) ;
\end{tikzpicture}
\end{figure}


Hierbei sind $q^{(i)}$ die Ladungen und $\mathbf{r}^{(i)}$ die Mittelpunkte der Teilchen. Ausgehend von diesem Beispiel werden die Wechselwirkungen in unserem Partikelsystem durch eine \textit{verallgemeinerte Coulomb-Kraft} beschrieben:
\begin{align*}
	\mathbf{f}_c = c_0 \cdot \frac{\mathbf{r}}{|\mathbf{r}|^{c_1}} \hspace{0.5cm} c_0\in\mathbb{R}, c_1\in\mathbb{R}_{\geq 0}
\end{align*}

Die abstoßenden Kräfte der Partikel untereinander sind somit durch 
\begin{align*}
	\mathbf{f}^{(i)}_{-} &= a_{-} \cdot \sum \mathbf{f}^{(i)}_m \\
	&=  a_{-} \cdot \sum_{m \in M_-} \frac{\mathbf{s}^{(i)} - \mathbf{s}^{(j)}}{|\mathbf{s}^{(i)} - \mathbf{s}^{(j)}|^{\alpha_-}}
\end{align*}
gegeben (davon ausgehend, dass alle Partikel den selben Ladungsbetrag haben). Die Formulierung der anziehenden Kraft ist leider nicht ganz so offensichtlich, da hierzu die interagierenden Partikel verschiedene Ladungsvorzeichen haben müssen. Um diese Ungereimtheit zu umgehen wird eine neue Ladung im Punkt $\tilde{\mathbf{s}}$, dem \textit{geometrischen Schwerpunkt} der Gruppe, eingeführt. Dadurch ergibt sich
\begin{align*}
	\mathbf{f}^{(i)}_{+} &= a_{+} \cdot \frac{\mathbf{s}^{(i)} - \tilde{\mathbf{s}}} {|\mathbf{s}^{(i)} - \tilde{\mathbf{s}}|^{\alpha_+}} \\
	&= a_{+} \cdot \frac{\mathbf{s}^{(i)} - \frac{1}{N} \sum_{j=1}^N\mathbf{s}^{(j)}}{|\mathbf{s}^{(i)} - \frac{1}{N} \sum_{j=1}^N\mathbf{s}^{(j)}|^{\alpha_+}}
\end{align*}

Äquivalent zur Abstoßenden Kraft der Schafe untereinander lässt sich der Einfluss eines Schäferhunds nun durch eine abstoßende Ladung (d.h. mit gleichem Vorzeichen) beschreiben:
\begin{align*}
	\mathbf{f}_h^{(i)} &= a_h \cdot  \frac{\mathbf{s}^{(i)} - \mathbf{h}} {|\mathbf{s}^{(i)} - \mathbf{h}|^{\alpha_h}}
\end{align*}
Es sei bemerkt, dass der Hund selbst nicht durch die Gruppe beeinflusst wird, sondern das Wirken einer äußeren Kraft darstellt. Im Fall mehrerer Hunde ist es sinnvoll, die Konstanten $(a_h,\alpha_h)$ für jeden Hund separat zu wählen.\\


\textit{Anmerkung: } Die Bewegung der Schafherde wird durch ein System von 2N Differentialgleichungen zweiter Ordnung beschrieben und hängt insgesamt von 6 Parametern $(a_+,a_-,a_h,\alpha_+,\alpha_-,\alpha_h)$ ab. Die Gleichungen sind homogen, falls $a_h = 0$, d.h. wenn kein Hund anwesend ist. Sie sind linear, falls $\alpha_+ = \alpha_- = \alpha_h = 0$.

\newpage

%%
% Interpretation, Bewertung, Randbedingungen, Konsistenz des Problems, ...
%
\section*{Revision des Modells und Ausblick}
Nach der Formulierung des mathematischen Problems stellen sich natürlich eine Vielzahl an Fragen bezüglich Stabilität, Konsistenz und Erweiterungsmöglichkeiten des Modells:

\begin{description}

	\item[Wahl der Anfangsbedingungen]{:\\ Da das Problem als System von 2N Differentialgleichungen zweiter Ordnung formuliert wurde, müssen 4N Anfangs- order Randbedingungen formuliert werden. Eine natürliche Wahl ist die Bewegungslosigkeit ($\left. \dot{\mathbf{s}}^{(i)} \right. \vline_{t=0} = \mathbf{0}$) zum Zeitpunkt $t=0$ und eine zufällige Startverteilung der Schafe (z.b. Gleichverteilung). }
	\item[Wahl der Parameter]{ :\\In der \textit{homogenen} Gleichung ist die Anzahl der Parameter konstant, d.h. unabhängig von der Anzahl der Partikel/Schafe. Jedoch beeinflussen diese die Dynamik des Systems maßgeblich. Eine besondere Rolle spielen die Parameter $\alpha_\star$, da sie die Beträge der wirkenden Kräfte in Relation zur Distanz der Partikel setzen. Offensichtlich sind $a_+,a_h \leq 0$ und $a_- \geq 0$ zu wählen; weitere Einschränkungen lassen sich allgemein nicht treffen. Die Wahl der $\alpha_\star$ ist etwas komplizierter. ($\ldots$) }
	\item[Stabilität der Lösungen]{:
		\begin{itemize}
			\item{ In welchem Maß hängt eine Lösung von der Wahl der Anfangsbedingungen ab?}
			\item{ Gibt es stabile Zustände (Fixpunkte, Zyklen, $\ldots$) ? }
			\item{ Falls es diese gibt: In welchem Maß (bzgl. welcher Norm, wie schnell) konvergieren nahe gelegene Trajektorien gegen diese stabilen Lösungen? }
			\item{ Gibt es immer eine Lösung, und ist diese eindeutig ? }
			\item{ Hat die Wahl der Parameter qualitativen Einfluss auf das Verhalten des Systems (Bifurkation)? }
	\end{itemize}			
	}
	\item[Modellierung von Hindernissen]{:\\ Wie lassen sich \textit{Hindernisse} sinnvoll in das Modell einbauen? Zwei Möglichkeiten wären 
	\begin{itemize}
		\item{das Einführen weiterer statischer, abstoßender Kräfte.}
		\item{das Formulieren algebraischer Nebenbedingungen, d.h. exakte Beschreibung der Geometrie des Areals, auf dem sich die Schafe bewegen dürfen.} 
	\end{itemize}}
	\item[Konsistenz des Modells]{:\\ Erfüllt das bisher entwickelte Modell überhaupt die Bedingungen, welche an das Verhalten der Schafherde gestellt werden? Beispielsweise sollte die abstoßende Kraft jedes einzelnen Schaf auch sicher stellen, dass die Schafe nicht miteinander \textit{kollidieren}. Ist dies nicht erfüllt, müssen weitere Nebenbedingungen eingeführt werden.}
\end{description}

Erst nachdem die obigen Fragen ausreichend geklärt wurden macht es Sinn, über eine Erweiterung der Problemstellung zur \textit{optimalen Kontrolle} der Schafherde nachzudenken. Diese beinhaltet unter anderem: 
\begin{description}
	\item[Maß für die Optimalität]{:\\ Im bisherigen Kontext ist überhaupt noch nicht klar, wann eine Lösung als optimal gelten soll. Im Fall der Existenz von Zyklen oder Fixpunkten wäre es beispielsweise möglich, diese durch die Bewegung des Hundes so zu verschieben, dass sie in ein bestimmtes Gebiet fallen. Gibt es keine solche stabilen Zustände, müssen andere Kriterien gefunden werden (z.b. Methoden aus der Maßtheorie/Stochastik ?).}
	\item[Verhalten der Hunde]{: \\ Lassen sich die Hunde direkt steuern (dies Bedeutet eine starke Vereinfachung des echten Verhaltens der Hunde !!!), und wie lassen sich unnatürliche Bewegungsmuster ausschließen?}
	\item[Modellverifikation]{: \\ Da keine Daten vorliegen, ist eine \textit{direkte} Verifikation des Modells nicht möglich. Wie lässt sich mit Hilfe der verfügbaren Daten (Bilder, Videos) sicherstellen, dass die gefundene Lösung realistisch ist?}
\end{description} 
\end{document}