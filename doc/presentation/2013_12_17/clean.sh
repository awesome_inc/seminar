#!/bin/bash

verz=$(dirname $0)

for i in *.backup *~ *.aux *.bm *.log *.old *.out *.toc *.blg *.swp *.ps *.dvi *-swp *.swp *.nav *.snm;
do
  find ${verz} -name ${i} -exec rm {} \;
done
