function [P,Phi] = SDE1(P0,Q,dt,px,py)
    P = zeros(size(P0));
    
    persistent pp dp;
    if(isempty(pp) && isempty(dp))
        % Berechne Spline f�r Potential
        pp = csape(px,py,'variational');
        % Berechne Ableitung des Potentials
        dp = fnder(pp);
    end
    
    N = size(Q,1)/2;

    % Unschärfe, momentan ~N0,c*dt)
    dW = dt*randn(2*N,1)*0;
    
    Qdiff = zeros(2*N-2,1);
    Phi = zeros(N-1,N-1);
    
    % Berechne �nderung der Geschwindigkeit f�r jedes Schaf
    for i=1:N
        % Qdiff_ij = Q_i - Q_j
        Qdiff(1:N-1) = Q([1:(i-1),(i+1):N])' - Q(i);
        Qdiff(N:2*N-2) = Q([(N+1):(N+i-1),(N+i+1):2*N])' - Q(N+i);
        % Werte Gradienten des Potentials aus
        XX = sqrt(Qdiff(1:N-1).^2 + Qdiff(N:2*N-2).^2);
        ZZ = -1*ppval(pp, XX);
        % ZZ = zeros(N-1,1);
        % ZZ(abs(XX) <= 1) = -1;
        % ZZ(abs(XX) > 1) = 0.1;
        ZZ(abs(XX) >= max(abs(px))) = 0;
        
        if nargout == 2
            Phi(i,:) = ZZ;
        end
        
        % F_ij = - grad.Phi(Q_j-Q_i) = grad.Phi(Q_i - Q_j)
        % P(n+1) = P(n) + dt*(dW + F)
        P(i) = P0(i) + dt*(dW(i)) +sum(ZZ./ XX .* Qdiff(1:N-1));
        P(N+i) = P0(N+i) + dt*(dW(N+i)) + sum(ZZ./ XX .* Qdiff(N:2*N-2)); 
    end
    
end

