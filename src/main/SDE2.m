%% ---- SDE2.m v. 0.1.0 21.12.2013 (Ehmann, Hans, Kluender, Linn, Ye) ----------
%%
%% ---- SYNTAX:
%% SDE2(P,                                    Impuls: <Vektor 2*N x 1>
%%      Q,                                    Ort:    <Vektor 2*N x 1>
%%     )

function Z = SDE2(Z,wkt_con,wkt_new)

N = size(Z,1);

%%% --- !!! editierbare Variablen: !!! ----------------------------------------------------------------
%wkt_con = .2;                                      % Wkt. dafuer, dass ein Schaf sich bewegt
%wkt_new = .4;                                      % Wkt. dafuer, dass ein stehendes Schaf losläuft
%%% ---------------------------------------------------------------------------------------------------

R = rand(N,1);
Z = Z.*round(R + (wkt_con - .5)) + (1-Z).*round(R + (wkt_new - .5));

