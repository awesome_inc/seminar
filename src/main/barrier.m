function Q = barrier(Q,Qstep,V,dt,Co,Tr,dx,dy,nx,ny)

N=size(Q,1)/2;

for i = 1:N
a=0;
S=[Q(i),Q(N+i)];
%    E=[Q(i,t),  Q(N+i,t)  ];
F=[Qstep(i),  Qstep(N+i)  ];
steplength = norm(F-S,2);
E=S + (F-S)*min(4,max(2,V(i))/dt)/steplength ;
E0=E;
F0=F;

C = lineIntersect(Co,dx,dy,nx,ny,S,E0);
l=length(C);
k=sum(any(ismember(C',Tr),2)); 
a1 = 0;
while l-k > 0 && abs(a1) <= pi
  a1= a1+pi/18;
  E = S+(E0-S)*[cos(a1), -sin(a1); sin(a1), cos(a1)];
  C = lineIntersect(Co,dx,dy,nx,ny,S,E);
  l = length(C);
  k = sum(any(ismember(C',Tr),2));
  
end

C = lineIntersect(Co,dx,dy,nx,ny,S,E0);
l=length(C);
k=sum(any(ismember(C',Tr),2)); 
a2 = 0;
while l-k > 0 && abs(a2) <= pi
  a2= a2-pi/18;
  E = S+(E0-S)*[cos(a2), -sin(a2); sin(a2), cos(a2)];
  C = lineIntersect(Co,dx,dy,nx,ny,S,E);
  l = length(C);
  k = sum(any(ismember(C',Tr),2));
end

if abs(a1)<abs(a2)
  F=S+(F0-S)*[cos(a1), -sin(a1); sin(a1), cos(a1)];
else
  F=S+(F0-S)*[cos(a2), -sin(a2); sin(a2), cos(a2)];
end

Q(i)  = F(1);
Q(N+i)= F(2);
end

