function [x,v,p,q]=changeDirection(x,v,p,q,vnew,tlast,tcurr,m)
N=size(x,1);
x(m,:)=x(m,:)+(tcurr-tlast)*v(m,:);
v(m,:)=vnew;
for k=1:N
    q(m,k,:)=x(m,:)-x(k,:);
    p(m,k,:)=v(m,:)-v(k,:);
    q(k,m,:)=x(k,:)-x(m,:);
    p(k,m,:)=v(k,:)-v(m,:);
end


