function boolean=checkDistances(xvec,dmin,tol)
N=size(xvec,1);
distances=inf*ones(N,N);
distancesMod=zeros(N,N);
for i=1:N
	for j=1:i-1
		distances(i,j)=norm([xvec(i,1)-xvec(j,1),xvec(i,2)-xvec(j,2)]);
        distancesMod(i,j)=norm([xvec(i,1)-xvec(j,1),xvec(i,2)-xvec(j,2)]);
	end
end
matmin=min(min(distances));
matmax=max(max(distancesMod));
tol2=10^(-5);
% minimalDistance=round(matmin/tol)*tol

if matmin<dmin && matmin>dmin-tol
     minimalDistance=dmin;
else 
     minimalDistance=matmin; 
end
distances=[minimalDistance,matmax];

%2-minimalDistance

if matmin>=dmin-tol 
	boolean=1;
else
	boolean=0;
end

end
