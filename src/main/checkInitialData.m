%
function []=checkInitialData(N,rphy,width,height,tstep,runningTime,tol)

lowerLimit=1; scalarCheck=1; lowerLimit=1; integerCheck=1;
standardErrors(N,scalarCheck,lowerLimit,integerCheck);

lowerLimit=tol; integerCheck=0;
standardErrors(rphy,scalarCheck,lowerLimit,integerCheck);
standardErrors(width,scalarCheck,lowerLimit,integerCheck);
standardErrors(height,scalarCheck,lowerLimit,integerCheck);
standardErrors(tstep,scalarCheck,lowerLimit,integerCheck);
lowerLimit=tstep;
standardErrors(runningTime,scalarCheck,lowerLimit,integerCheck);

%
function []=standardErrors(variable,scalarCheck,lowerLimit,integerCheck)
%
if scalarCheck==1 && isscalar(variable)==0
	error('No scalar input for an argument which should be scalar.');
elseif variable < lowerLimit
	error('Input is below its reasonable lower limit.');
elseif integerCheck==1 && mod(variable,1)~=0
	error('Input is not integer, but should be.');
elseif variable==NaN
	error('Input is NaN (not a number).');
elseif variable==inf
	error('Input is infinity.');
end
