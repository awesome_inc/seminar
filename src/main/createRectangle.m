function [Coord, Tr] = createRectangle(length_x, length_y, triangle_size)
% Erstellt gleichm��ige Triangulierung eines Rechtecks
% INPUT     (double) length_x        : Gr��e in x-Richtung
%           (double) length_y        : Gr��e in y-Richtung
%           (double) triangle_size   : Maximale Seitenl�nge der Dreiecke
%
% OUTPUT    (double)Coord   : N x 2 Matrix (Knoten)
%           (int)Tr         : M x 3 Matrix (Dreiecke)

    % Bestimme Anzahl der Knoten
    nx = ceil(length_x/triangle_size)+1;
    ny = ceil(length_y/triangle_size)+1;
    
    % Rasterung des Rechtecks
    xcoord = linspace(0,length_x,nx);
    ycoord = linspace(0,length_y,ny);
    
    N = nx * ny; 
    M = (nx-1) * (ny-1) * 2; 
    
    Coord = zeros(N, 2);
    Tr = zeros(M, 3);
    
    % Erzeuge Koordinaten-Matrix
    for i = 1:ny;
        Coord((1+(i-1)*nx:i*nx), 1) = xcoord;
        Coord((1+(i-1)*nx:i*nx), 2) = ycoord(i)*ones(nx,1);
    end
    
    % Laufe Raster zeilenweise ab, um Dreiecke zu erzeugen
    for i = 1:ny-1;
        % lower triangles
        Tr(2*(i-1)*(nx-1) + (1:2:2*(nx-1)),1) = (i-1)*nx + (1:nx-1);
        Tr(2*(i-1)*(nx-1) + (1:2:2*(nx-1)),2) = (i-1)*nx + (2:nx);
        Tr(2*(i-1)*(nx-1) + (1:2:2*(nx-1)),3) = i*nx + (1:nx-1);
        
        % upper triangle
        Tr(2*(i-1)*(nx-1) + (2:2:2*(nx-1)),1) = (i-1)*nx + (2:nx);
        Tr(2*(i-1)*(nx-1) + (2:2:2*(nx-1)),2) = i*nx + (1:nx-1);
        Tr(2*(i-1)*(nx-1) + (2:2:2*(nx-1)),3) = i*nx + (2:nx);
    end
end

