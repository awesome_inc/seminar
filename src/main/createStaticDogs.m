function [D,Vh] = createStaticDogs(id,Q0,tstep,niterations)

    N = size(Q0,1)/2;
    switch id
        case 0
            Vh = repmat([0;0],1,niterations); 
            D = repmat([-50;-50],1,niterations);
        case 1
            D0 = [max(Q0(1:N));max(Q0((N+1):2*N))] + [5;5];
            Vh = [-2.5;-2.5];
            D = [D0(1)+(0:tstep:niterations*tstep)*Vh(1);D0(2)+(0:tstep:niterations*tstep)*Vh(2)];
            Vh = repmat(Vh,1,niterations);
        case 2
            D01 = [max(Q0(1:N));max(Q0((N+1):2*N))] + [20;3];
            D02 = [max(Q0(1:N));min(Q0((N+1):2*N))] + [20;-3];
            Vh1 = [-3;0];
            Vh2 = [-3;0];
            D1 = [D01(1)+(0:tstep:niterations*tstep)*Vh1(1);D01(2)+(0:tstep:niterations*tstep)*Vh1(2)];
            D2 = [D02(1)+(0:tstep:niterations*tstep)*Vh2(1);D02(2)+(0:tstep:niterations*tstep)*Vh2(2)];
            D = [D1(1,:);D2(1,:);D1(2,:);D2(2,:)];
            Vh = repmat([Vh1(1);Vh2(1);Vh1(2);Vh2(2)],1,niterations); 
            
        case 3
            Vh11 = [-2;0];
            Vh12 = [0;-2];
            Vh21 = [0;-2];
            Vh22 = [-2;0];

            tstop = floor(niterations/2);
            t1 = linspace(0,tstop*tstep,tstop);
            t2 = linspace(0,(tstop+mod(niterations,2))*tstep,tstop+mod(niterations,2));
            
            D01 = [max(Q0(1:N));max(Q0((N+1):2*N))] + [4;4];
            D02 = [max(Q0(1:N));max(Q0((N+1):2*N))] + [6;2];
            D11 = [D01(1)+t1*Vh11(1);D01(2)+t1*Vh11(2)];
            D21 = [D02(1)+t1*Vh21(1);D02(2)+t1*Vh21(2)];
            D12 = [D11(1,end)+t2*Vh12(1);D11(2,end)+t2*Vh12(2)];
            D22 = [D21(1,end)+t2*Vh22(1);D21(2,end)+t2*Vh22(2)];
            D = [D11(1,:) D12(1,:);D21(1,:) D22(1,:);D11(2,:) D12(2,:);D21(2,:) D22(2,:)];
            Vh = [repmat([Vh11(1);Vh21(1);Vh11(2);Vh21(2)],1,tstop), ...
                repmat([Vh12(1);Vh22(1);Vh12(2);Vh22(2)],1,tstop+mod(niterations,2))]; 
            
         case 4
            D0 = [max(Q0(1:N));max(Q0((N+1):2*N))] + [10;10];
            Vh1 = [-2.5;-2.5];
            Vh2 = [-3;0];
            
            tstop = floor(niterations/3);
            t1 = linspace(0,tstop*tstep,tstop);
            t2 = linspace(0,(2*tstop+mod(niterations,3))*tstep,2*tstop+mod(niterations,3));
            
            D11 = [D0(1)+t1*Vh1(1);D0(2)+t1*Vh1(2)];
            D12 = [D11(1,end)+t2*Vh2(1);D11(2,end)+t2*Vh2(2)];
            
            Vh = [repmat(Vh1,1,tstop),repmat(Vh2,1,2*tstop+mod(niterations,3))];
            D = [D11 D12];
          
    end

end