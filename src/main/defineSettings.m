% Input:
%   filename
%   DEBUG
%   tstep
%   runningTime

function defineSettings(varargin)
    
    if nargin>4
        error('Maximal number of optional arguments in main is 4.');
    end

    
    % Standardwerte 
    filename = 'defaultSettings';
    DEBUG = 0; % DEBUG Modus
    tstep=0.1; runningTime=30; % Zeitschritt und Gesamtzeit

    if nargin>=1
        filename=varargin{1};
    end
    if nargin>=2
        DEBUG=varargin{2};
    end
    if nargin>=3
        tstep=varargin{3};
    end
    if nargin>=4
        runningTime=varargin{4};
    end
    
    save(['settings/',filename],'DEBUG','tstep','runningTime');
end