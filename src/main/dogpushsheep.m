function Vs = dogpushsheep(Pha,Vh,Psa,d,vmax,ma)
% Ps ist Positon von Schäfe, Pha ist Anfangsposition von Hund,
% Vh ist Velocity von Hund, Psa ist Anfangspositon von Schäfe
% d ist radius der Kreis
% vmax ist die Max_velocity
% der Schäfe
% t ist die Länge der Zeitschritte

Ns = size(Psa,1)/2;                                % N ist das Anzalh der Schäfe
Nh = size(Vh, 1)/2;                                % N ist das Anzalh der Hunde

Ps = Psa;
A=zeros(2,Ns);                       % Angstlevel für Ns Schäfe
Ablaufrichtung = zeros(2,Nh);
Abstand=ones(Nh,1);
Vs = zeros(2,Ns);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for j = 1:Ns
  for k = 1:Nh
    Ablaufrichtung(:,k) = [Psa(j)-Pha(k);Psa(j+Ns)-Pha(k+Nh)];   % Ablaufrichtung von Hund k
    Abstand(k) = norm(Ablaufrichtung(:,k),2);                                  % Abstand zwischen Schaf j und Hund k
    
    vproj = Vh([k Nh+k])'*Ablaufrichtung(:,k)/Abstand(k);
    if Abstand(k)  < d
        A(:,j)=A(:,j) + ma*max(vproj,0)/(Abstand(k)+10)*Ablaufrichtung(:,k)/Abstand(k);
    end
    if Abstand(k) < 0.5*d
        A(:,j)=A(:,j) + 1/(Abstand(k)+10)*Ablaufrichtung(:,k)/Abstand(k);
    end
  end

    Vs(:,j) = A(:,j);
end

	Vs = reshape(Vs',2*Ns,1);
