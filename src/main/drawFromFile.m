function drawFromFile(filename)
    close all;
    figure('Name',['Simulation ',filename]);
    load(['results/',filename]);
    draw(Q,D,Co,H,rphy);
end