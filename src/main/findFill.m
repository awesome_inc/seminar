function fillPercentage=findFill(t)
% Nur zu Testzwecken.
N=size(t,1);
testFill=zeros(N,N); 
notInf=0;
for k=1:N
	for l=1:N
		if t(k,l) ~=inf
			testFill(k,l)=t(k,l);
			notInf=notInf+1;
		end
	end
end
%spy(testFill);
fillPercentage=notInf/(N^2);