function tsvec=findRendezvous(pvec,qvec,dphy,tcurr,tstep,tol)

N=size(pvec,1);
tsvec=zeros(N,1);
a=pvec(:,1).^2+pvec(:,2).^2;
b=2*(pvec(:,1).*qvec(:,1)+pvec(:,2).*qvec(:,2));
c=qvec(:,1).^2+qvec(:,2).^2-dphy^2;
for i=1:N
    polynomial=[a(i),b(i),c(i)];
	% The next if is just to stop Octave warnings.
	if a(i)~=0 || b(i)~=0 || c(i)~=0
		zerosPoly=roots(polynomial);
	else
		zerosPoly=[inf,inf];
	end
			
	if isempty(zerosPoly)
        t1=inf; t2=inf;
    elseif length(zerosPoly)==1
        t1=zerosPoly; t2=inf;
    else
        t1=zerosPoly(1); t2=zerosPoly(2);
    end
    if imag(t1)~=0
        t1=inf; t2=inf;
    else 
        if t1<-tol || t1>tstep+tol
            t1=inf;
        end
        if t2<-tol || t2>tstep+tol
            t2=inf;
        end
    end 
    tCandidate=min([t1,t2]);
    % Negative Steigung von poly=a(i)t^2+b(i)t+c(i) im Punkt tCandidate ?
    % Nur dann liegt eine echte Kollision vor!
    if a(i)*tCandidate+b(i)/2<tol
        tsvec(i)=tCandidate;
    else
        tsvec(i)=inf;
    end
end
