function [ncomps,comps]=graphconncomp3(G)
% This is an implementation from the pseudocode at 
% https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm.

% Preliminaries
if size(G,1)~=size(G,2)
    error('Input has to be a square matrix');
end
N=size(G,1);

firsttime=0;
for v=1:N
	for w=1:v-1
		if G(v,w)~=G(w,v);
			G(w,v)=G(v,w);
			firsttime=1;
		end
	end
end
if firsttime==1
	warning('graphconncomp3: Graph not symmetric. Upper triangular matrix will be overwritten.');
end

% Start of real work
counter=1; k=0;
parameters=zeros(N,4);
ncomps=0; comps=zeros(N,1);

for v=1:N
	if parameters(v,3)==0
		[comps,ncomps,parameters,counter,k]=connect(comps,ncomps,G,v,parameters,counter,k);
	end
end

end

% -----------------------------------------------------------

function [comps,ncomps,parameters,counter,k]=connect(comps,ncomps,G,v,parameters,counter,k)
N=size(G,1);
% stack=parameters(:,1); instack=parameters(:,2);
% index=parameters(:,3); lowlink=parameters(:,4);
 
% Set the depth index for v to the smallest unused index =counter
parameters(v,3)=counter;
parameters(v,4)=counter;
counter=counter+1;
% Next 3 lines: push stack.
k=k+1;
parameters(k,1)=v;
parameters(v,2)=1;

% Consider successors of v
for w=1:N
	if G(v,w)==1
		if parameters(w,3)==0 % w was not visited so far, recursion on w
			[comps,ncomps,parameters,counter,k]=connect(comps,ncomps,G,w,parameters,counter,k);
			parameters(v,4)=min(parameters(v,4),parameters(w,4));
		elseif parameters(w,2)==1 % w is in stack and hence in connected component
			parameters(v,4)=min(parameters(v,4),parameters(w,3));
		end
	end
end

% If v is a root node, pop the stack and generate a connected component
if parameters(v,4)==parameters(v,3)
	% Start new connected component.
	ncomps=ncomps+1;
	while true
		% Pop stack.
		w=parameters(k,1);
		parameters(w,2)=0;
		k=k-1;
		% Add w to current connected component.
		comps(w)=ncomps;
		if w==v
			break;
		end
	end
end

end

% algorithm tarjan is
  % input: graph G = (V, E)
  % output: set of strongly connected components (sets of vertices)

  % index := 0
  % S := empty
  % for each v in V do
    % if (v.index is undefined) then
      % strongconnect(v)
    % end if
  % end for

  % function strongconnect(v)
    % // Set the depth index for v to the smallest unused index
    % v.index := index
    % v.lowlink := index
    % index := index + 1
    % S.push(v)

    % // Consider successors of v
    % for each (v, w) in E do
      % if (w.index is undefined) then
        % // Successor w has not yet been visited; recurse on it
        % strongconnect(w)
        % v.lowlink  := min(v.lowlink, w.lowlink)
      % else if (w is in S) then
        % // Successor w is in stack S and hence in the current SCC
        % v.lowlink  := min(v.lowlink, w.index)
      % end if
    % end for

    % // If v is a root node, pop the stack and generate an SCC
    % if (v.lowlink = v.index) then
      % start a new strongly connected component
      % repeat
        % w := S.pop()
        % add w to current strongly connected component
      % until (w = v)
      % output the current strongly connected component
    % end if
  % end function
