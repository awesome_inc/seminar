function [DEBUG,rphy,tstep,runningTime]= handleInput(input)
largin=length(input);
%celldisp(input)

if largin>5
	error('Maximal number of optional arguments in main is six.');
end

% Standardwerte 
DEBUG = 0; % DEBUG Modus
rphy=0.5;% physischer Mindestabstand
tstep=0.5; runningTime=30; % Zeitschritt und Gesamtzeit

if largin>=1 
	DEBUG=input{1};
end
if largin>=2
	tstep=input{3};
end
if largin>=3
	runningTime=input{4};
end

