% Input: 
%   geometry_id
%   gridsize_init
%   N_sheep
%   start_seed
%   gridsize_sim
%
function [Co,Tr,Tx,Q0,dx,dy,nx,ny] = initialData(varargin) 
    if nargin==5
        [Co,Tr,~,~,~,~,~] = createGeometry(varargin{1},varargin{2});
        Q0=createStart(varargin{3},Co,Tr,findNodes(Co,varargin{4}));
        [Co,Tr,Tx,dx,dy,nx,ny] = createGeometry(varargin{1},varargin{5});
    elseif nargin==1
        assert(exist(['data/',varargin{1},'.mat'],'file')>0,'Datei existiert nicht');
        load(['data/',varargin{1},'.mat']);
    else
        error('Fehler');
    end

end