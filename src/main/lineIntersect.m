% lineIntersect, 
% Version 1.0r1 , 2014-01-26, Dominik Linn
%
% Berechnet Approximation einer Strecke in einem gerasterten oder 
% trianguliertem Gebiet.
%
%INPUT:    Coord   Koordinatenvektor
%           dx, dy  Rastergr��e
%           nx, ny  Anzahl der Rasterpunkte
%           P1, P2  Start- und Endpunkt der Strecke
%(optional) Tr      Triangulation, 
% 
%OUTPUT:    Co      Approximation der Strecke durch Rasterpunkte
%(optional) Trx     Approximation der Strecke durch Dreiecke
%
% 
function Co = lineIntersect(Coord,dx,dy,nx,ny,P1,P2,Tr)
    % Steigung
    m = (P2(2)-P1(2))./(P2(1)-P1(1));
    M = abs(m * dx/dy);
    
    % finde Knoten, der Starpunkt am n�chsten liegt (nicht
    % vektorisierbar?!?)
    id_1 = (Coord(:,1)<=P1(1)) & ((Coord(:,1)+dx)>P1(1)) & (Coord(:,2)<=P1(2)) & ((Coord(:,2)+dy)>P1(2));
    
    Co1 = find(id_1);
    %Co2 = find(id_2);
    
    sx = sign(P2(1)-P1(1));
    sy = sign(P2(2)-P1(2));
    Nx = 1 + floor(abs(P2(1) - P1(1))/dx);
    Ny = 1 + floor(abs(P2(2) - P1(2))/dy);
    
   
    if M==0
        Co = Co1 + (0:sx:sx*(Nx-1));
    elseif isinf(M)
        Co = Co1 + nx*round(0:sy*M:sy*M*(Nx-1));
    elseif M<=1
        Co = Co1 + (0:sx:sx*(Nx-1)) + nx*round(0:sy*M:sy*M*(Nx-1)); 
    else
        Co = Co1 + nx*(0:sy:sy*(Ny-1)) + round(0:sx/M:sx*(Ny-1)/M);
    end
    
%     varargout{1} = Co;
%     
%     if nargout>1
%         assert(nargin == 8, 'Mehr Parameter ben�tigt');
%         varargout{2} = Tr(sum(ismember(Tr,Co),2)>0,:);
%     end 
end