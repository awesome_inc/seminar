% Simulation einer Schafherde
%
% VERSION 1.0.1
%
% Letztmals geaendert: 26.01.2014, Linn
%
% INPUT(optional)   D       DEBUG MODE (0,1)
%                   N       Anzahl der Schafe
%                   tstep   Länge der Zeitschritte
%                   T       Simulationsdauer
%
function main(id_h,initdata, settings, output)
% Schliesse alle Fenster
close all;
clear SDE1 SDE2;

if(~exist('csape'))
    pkg load splines;
end

if exist(['settings/',settings,'.mat'],'file')==2
    load(['settings/',settings,'.mat'])
else
    rphy = 0.5;
    vmax = 2; % Maximalgeschwindigkeit der Schafe
    w_con = 0.2;
    w_new = 0.4;
    d_Hund=20; %% Einflussradius (Hund)
    mult_Hund = 10;    
    % Potential-Parameter
    v_ab = 2;   % abstoßendes Potential
    v_an = -1;  % anziehendes Potential  
    mult_Pot = 2;
    enable_move = 1;
    enable_barrier = 1;
    settings = 'default';
end

[Co,Tr,H,Q0,dx,dy,nx,ny] = initialData(initdata);

N = size(Q0,1)/2;
width =max(Co(:,1));
height=max(Co(:,2));

tol=10^-10; % Toleranz oder Gitterweite (fuer Vergleiche)
checkInitialData(N,rphy,width,height,tstep,runningTime,tol);

MC_iter = 20;

% Hund-ID

dphy=2*rphy; % physischer Abstand
niterations=floor(runningTime/tstep); % Anzahl der Iterationen

% NICHT VERÄNDERN!!! Werte sind sensibel.
%px = 1.2*[-25 -20 -15 -11.5 -7*rphy 0 7*rphy 11.5 15 20 25];
%py = 1/log(N)*[0 0.3*v_an 0.6*v_an v_an 0.6*v_an 0.3*v_an 0 v_ab 0 0.3*v_an 0.6*v_an v_an 0.6*v_an 0.3*v_an 0];

px = mult_Pot*[-10:2.5:-2.5 -2 0 2 2.5:2.5:10];
py = 1/sqrt(log(N))*[0 0.5*v_an 0.5*v_an 0 0.2*v_ab v_ab 0.2*v_ab 0 0.5*v_an 0.5*v_an 0];

Q=zeros(2*N,niterations); % Positionen der N Schafe
Q(:,1)=Q0;

P=zeros(2*N,niterations);
Z=zeros(N,1);

pp = csape(px,py,'variational');
Error_Phi = zeros(niterations,1);
phimax = ppval(pp,0);

%% DEBUG INFORMATIONEN
if DEBUG
    % Modell-Daten
    fprintf(' ==== MODELL-DATEN ==== \n');
    fprintf('Anzahl der Schafe: \t%d\n',N);
    fprintf('Kollisionsradius der Schafe: \t%f\n',rphy);
    fprintf('Größe des Gebiets: \t%f x %f \n',width,height);
    if size(H,1)>0
        fprintf('Hindernisse: \t Ja\n');
    else
        fprintf('Hindernisse: \t Nein\n');
    end
    fprintf('Potential-Vektor: \n ')
    disp(px);
    disp(py);
    
    
    % Simulations-Parameter
    fprintf('\n ==== SIMULATIONS-DATEN ==== \n');
    fprintf('Zeitvektor: \n');
    disp(0:tstep:runningTime);
    fprintf('\n Monte-Carlo Iterationen: \t%d \n',MC_iter);
    
    % Plotte 1D-Potential
    pxval = -50:0.1:50;
    pp = csape(px,py,'variational');
    pyval = ppval(pp,pxval);
    pyval(abs(pxval)>max(abs(px))) = 0;
    figure('Name','Potential');
    hold on;
    axis([-50 50 min(pyval)-1 max(pyval)+1]);
    plot(pxval,pyval,'-b');
    plot(px,py,'.r','LineWidth',2);
    hold off
end   

[D,Vh] = createStaticDogs(id_h,Q0,tstep,niterations);

tic
%% STARTE BERECHNUNG
for i=2:niterations
    P1 = zeros(2*N,1);
    Z1 = zeros(N,1);
    
    for mc = 1:MC_iter
        P1 = P1 + 1/MC_iter * SDE1(P(:,i-1),Q(:,i-1),tstep,px,py);
        Z1 = Z1 + 1/MC_iter*SDE2(Z,w_con,w_new);
    end
    
    Z = Z1>0.5;
    
    P1 = [Z;Z].*P1;
    
    % fuege Hund HIER ein
    PHund = dogpushsheep(D(:,i),Vh(:,i),Q(:,i-1),d_Hund,vmax,mult_Hund);
    %PHund = 0;
    
	% Normierung der Geschwindigkeiten
	P(:,i) = P1 + PHund;
	pnorm = sqrt(P(1:N,i).^2 + P((N+1):2*N,i).^2);
	id = pnorm > vmax;
	P([id id],i) = P([id id],i) ./ [pnorm(id);pnorm(id)] .* vmax;

	% Setze neuen Ort
	Q(:,i) = P(:,i)*tstep + Q(:,i-1);
    [~,Phi_temp] = SDE1(P(:,i),Q(:,i),tstep,px,py);
    
    % Weiche Hindernissen aus
    if enable_barrier
        Q(:,i) = barrier(Q(:,i-1),Q(:,i),sqrt(P(1:N,i).^2 + P((N+1):2*N,i).^2),tstep,Co,Tr,dx,dy,nx,ny);
    end   
    
    % Kollisionscheck
    if enable_move>0
        x = reshape(Q(:,i-1),N,2); 
        xtarget = reshape(Q(:,i),N,2); 
        v = (xtarget-x)/tstep;
        x = move(x,xtarget,v,dphy,tstep,tol);
        Q(:,i) = reshape(squeeze(x),2*N,1);
    end
    
    % absv=hypot(absvec(:,1),absvec(:,2)); % Fuer SDE1 und 2.
	%
    % ----------------------------------
    % Einfache Testvarianten
    %x=reshape(Q(:,i-1),N,2);; % Fuer xmean-Variante
    %absv=ones(N,1); % Fuer xmean-Variante und getTargets.
    
    %----------------------------------------------
    
    %draw(Q(:,i),D,Co,H,rphy);
    [~,Phi_corrected] = SDE1(P(:,i),Q(:,i),tstep,px,py);
    Error_Phi(i) = sum(sum(abs(Phi_temp-Phi_corrected)))/(N*N*phimax);
end
simtime = toc;

if nargin==4
    save(['results/',initdata,'_hund',num2str(id_h),'_',settings,'.mat'],'P','Q','Co','H','D','rphy','simtime');
    if output==1
        figure('Name','Simulation');
        draw(Q,D,Co,H,rphy);
        figure('Name','Error');
        hold on;
        semilogy(1:niterations,Error_Phi,'b-');
        semilogy(1:niterations,cumsum(Error_Phi),'r--');
        hold off;
    end
else
    figure('Name','Simulation');
    draw(Q,D,Co,H,rphy);
    figure('Name','Error');
    hold on;
    semilogy(1:niterations,Error_Phi,'b-');
    semilogy(1:niterations,cumsum(Error_Phi),'r--');
    hold off;
end