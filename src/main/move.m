function x=move(x,xtarget,v,dphy,tstep,tol)
%
N=size(x,1); 
p=zeros(N,N,2); q=zeros(N,N,2); t=zeros(N,N);
qnorm=zeros(N,N);
%tol2=10^(-5);

tcurr=0; tlast=0;
for k=1:N
    pvec=[v(:,1)-v(k,1),v(:,2)-v(k,2)];
    qvec=[x(:,1)-x(k,1),x(:,2)-x(k,2)];
    t(k,:)=findRendezvous(pvec,qvec,dphy,tcurr,tstep,tol);
    p(k,:,1:2)=pvec;
    q(k,:,1:2)=qvec;
end
ptarget=-v;  
qtarget=xtarget-x;
ttarget=findRendezvous(ptarget,qtarget,0,tcurr,tstep,tol);
%
%fillPercentage=findFill(t)
%

% Berechnung des neuen tcurr (Zeitpunkt der Kollision)
% t=round(t/tol)*tol; ttarget=round(ttarget/tol)*tol;
tmin=min(min(t));
[ttmin,ttindx]=min(ttarget);
[tcurr,toptions]=min([tmin,ttmin]);
if tcurr<tol
    tcurr=0;
end
vnorm=hypot(v(:,1),v(:,2));
vnormmod=vnorm;
for i=1:N
    if vnorm(i)==0
       vnormmod(i)=inf; 
    end
end
vpositive=min(vnormmod);

iteration=1;
while tcurr<tstep && iteration<=1000
    if iteration==1000
    	error('move: Maximal number of iterations reached.');
    end
    if toptions==1
        % Setze neue Positionen fuer alle Schafe.
        x=x+tcurr*v;
        %x=round(x/tol)*tol;
        tlast=tcurr;
        for i=1:N
            q(i,:,1:2)=[x(:,1)-x(i,1),x(:,2)-x(i,2)];
            qnorm(i,:)=hypot(squeeze(q(i,:,1)),squeeze(q(i,:,2)));
        end
        % Welche Schafe beruehren sich oder sind durch andere
        % Schafe miteinander verbunden ?
        graph=abs(qnorm-dphy)<tol;
        %connTest(graph);
        
%        [ncomps,comps]=graphconncomp(sparse(graph),'Directed',false);
		if exist('graphconncomp','file')==0
            [ncomps,comps]=graphconncomp3(graph);
		else
            [ncomps,comps]=graphconncomp(sparse(graph),'Directed',false);
		end
        [sorted,sortedindx]=sort(comps);
        firstEntry=zeros(ncomps+1,1);
        firstEntry(1)=1; k=2;
        for i=2:N
            if sorted(i)~=sorted(i-1)
                firstEntry(k)=i;
                k=k+1;
            end
        end
        firstEntry(ncomps+1)=N+1;
        % Setze neue Geschwindigkeiten fuer alle Schafe
        vnew=zeros(ncomps,2);
        for k=1:ncomps
            sheepindx=sortedindx(firstEntry(k):firstEntry(k+1)-1);
            vnew(k,:)=mean(v(sheepindx,:),1);
        end
        v(:,1:2)=vnew(comps(:),1:2);
        for i=1:N
             if norm(v(i,1:2))<10^-3
                v(i,1:2)=[0,0];
             end
        end
        % v=round(v/tol)*tol;
        for i=1:N
            p(i,:,1:2)=[v(:,1)-v(i,1),v(:,2)-v(i,2)];
        end
        ptarget=-v; qtarget=xtarget-x;
        % Neue Kollisionszeitpunkte und Ankunftszeiten.
        dist=dphy;
        t=tUpdate(p,q,dist,t,tcurr,tstep,tol);
        ttarget=findRendezvous(ptarget,qtarget,0,tcurr,tstep,tol);
        
    else % toptions==2
        % Schaf im Zielpunkt.
        % Setze neue Position und stoppe Schaf n.
        x=x+tcurr*v;
        tlast=tcurr;
        v(ttindx,:)=[0,0]; % Kein Runden notwendig! 
        for i=1:N
            q(i,:,1:2)=[x(:,1)-x(i,1),x(:,2)-x(i,2)];
            p(i,:,1:2)=[v(:,1)-v(i,1),v(:,2)-v(i,2)];
        end
        ptarget=-v; qtarget=xtarget-x;
        % Neue Kollisionszeitpunkte und Ankunftszeiten.
        dist=dphy;
        t=tUpdate(p,q,dist,t,tcurr,tstep,tol);
        ttarget=findRendezvous(ptarget,qtarget,0,tcurr,tstep,tol);
    end
    %
    % Berechnung des neuen tcurr (Zeitpunkt der naechsten Kollision)
    %t=round(t/tol)*tol; ttarget=round(ttarget/tol)*tol;
    tmin=min(min(t));
    [ttmin,ttindx]=min(ttarget);
    [tcurr,toptions]=min([tmin,ttmin]);
    if tcurr<tol
        tcurr=0;
    end
    
    % Inkrementieren
    iteration=iteration+1;
end

% Fuer diesen Zeitschritt abschliessende Iteration fuer tcurr=tstep.
% Setze neue Position, Geschwindigkeit aller Schafe wird beibehalten.
x=x+(tstep-tlast)*v;
% x=round(x/tol)*tol;

%
%
% Nur zu Testzwecken.
%checkDistances([newPos(:,1),newPos(:,2)],dphy,tol);
if checkDistances([x(:,1),x(:,2)],dphy,tol)==0
    warning('move: Minimal distance below physical distance.');
end
%pause(0.5);

end
% ---------------------------------------------------------------

function t=tUpdate(p,q,dphy,t,tcurr,tstep,tol)

N=size(p,1);
for m=1:N
    pvec=[p(m,:,1)',p(m,:,2)']; qvec=[q(m,:,1)',q(m,:,2)'];
    t(m,:)=findRendezvous(pvec,qvec,dphy,tcurr,tstep,tol)';
    pvec=[p(:,m,1),p(:,m,2)]; qvec=[q(:,m,1),q(:,m,2)];
    t(:,m)=findRendezvous(pvec,qvec,dphy,tcurr,tstep,tol);
    t(m,m)=inf;
end
end
% -------------------------------------------------------------------

