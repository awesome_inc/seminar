function [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners)
    bbx = findNodes(Coord,[corners(1,1) corners(2,1) corners(2,1) corners(1,1);...
        corners(1,2) corners(1,2) corners(2,2) corners(2,2)]');
    
    % degenerierter Fall (Punkt)
    if bbx(1)==bbx(2) && bbx(2)==bbx(3)
        warning('Rechteck zu klein! Keine Punkte ausgewählt!');
        return
    % horizontale Linie
    elseif bbx(1)==bbx(2)
        warning('Degeneriertes Rechteck (Linie)! Keine Punkte ausgewählt!');
        return
    % vertikale Linie
    elseif bbx(2)==bbx(3)
        warning('Degeneriertes Rechteck (Linie)! Keine Punkte ausgewählt!');
        return
    % Rechteck
    else
        nodes = find(Coord(:,1)>Coord(bbx(1),1) & Coord(:,1)<Coord(bbx(3),1) ...
                & Coord(:,2)>Coord(bbx(1),2)  & Coord(:,2)<Coord(bbx(3),2));
    end
    
    % entferne innere Punkte
    for i=1:length(nodes)
        Tx = [Tx;Tr(Tr(:,1)==nodes(i),:)];
        Tr(Tr(:,1)==nodes(i),:) = [];
        Tx = [Tx;Tr(Tr(:,2)==nodes(i),:)];
        Tr(Tr(:,2)==nodes(i),:) = [];
        Tx = [Tx;Tr(Tr(:,3)==nodes(i),:)];
        Tr(Tr(:,3)==nodes(i),:) = [];
    end

    % entferne Eckpunkte:

    % SW
    sel = find(Tr(:,1)==bbx(1) & Tr(:,2)==(bbx(1)+1));
    Tx = [Tx;Tr(sel,:)];
    Tr(sel,:) = [];
    % NE
    sel = find(Tr(:,3)==bbx(3) & Tr(:,2)==(bbx(3)-1));
    Tx = [Tx;Tr(sel,:)];
    Tr(sel,:) = [];

    [Coord,Tx] = removeDoubles(Coord,Tx);
end