function t=tUpdate(p,q,dphy,t,tcurr,tstep,tol,m)

pvec=squeeze(p(m,:,1:2)); qvec=squeeze(q(m,:,1:2));
t(m,:)=findRendezvous(pvec,qvec,dphy,tcurr,tstep,tol);
pvec=squeeze(p(:,m,1:2)); qvec=squeeze(q(:,m,1:2));
t(:,m)=findRendezvous(pvec,qvec,dphy,tcurr,tstep,tol);
t(m,m)=inf;
