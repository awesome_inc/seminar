function boolean=checkDistances(xvec,dmin,tol)
xlength=size(xvec,1);
distancesModified=zeros(xlength,xlength);
for i=1:xlength
	for j=1:xlength
		distancesModified(i,j)=norm([xvec(i,1)-xvec(j,1),xvec(i,2)-xvec(j,2)]);
		if j==i
			distancesModified(i,j)=inf;
		end
	end
end
round(10^10*min(min(distancesModified)))/(10^10);
%round(10^10*max(max(distancesModified)))/(10^10)
% dmin

if min(min(distancesModified))>=dmin-tol 
	boolean=1;
else
	boolean=0;
end
