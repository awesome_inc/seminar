% VERSION 0.5.1
%
% Letztmals geaendert: 12.12.2013, Stefan Kluender
%
function []=main(varargin)
tic
% Schliesse alle Fenster
close all;
pkg load splines;
%
tol=10^-10; % Toleranz oder Gitterweite (fuer Vergleiche)

input=varargin;
[N,rphy,width,height,tstep,runningTime]= handleInput(input);
checkInitialData(N,rphy,width,height,tstep,runningTime,tol);

dphy=2*rphy; % physischer Abstand
niterations=floor(runningTime/tstep); % Anzahl der Iterationen
maxOfStarts=100;
pos=zeros(N,niterations,2); % Positionen der N Schafe
pos(:,1,1:2)=createStart(N,width,height,dphy,maxOfStarts,tol);

draw(reshape(squeeze(pos(:,1,1:2)),2*N,1),width,height,rphy);
xmean=1/N*[sum(squeeze(pos(:,1,1))),sum(squeeze(pos(:,1,2)))];
xtarget=[xmean(1)*ones(N,1),xmean(2)*ones(N,1)];
P=zeros(2*N,niterations); Q=[reshape(squeeze(pos(:,1,1:2)),2*N,1) zeros(2*N,niterations-1)];

for i=2:niterations
	%xtarget=getTargets(x,tstep);
	P(:,i) = SDE1(P(:,i-1),Q(:,i-1),tstep);
	[P(:,i),Q(:,i)] = SDE2(P(:,i),Q(:,i-1),tstep);
	absvec=reshape(Q(:,i)-Q(:,i-1),N,2);
	for j=1:N 
		absv(j)=norm(absvec(j,1:2));
	end
	absv=ones(N,1); % Fuer xmean-Variante und getTargets.
	x = reshape(Q(:,i-1),N,2);
	xtarget=reshape(Q(:,i),N,2);
	Q(:,i)=reshape(squeeze(move(x,xtarget,absv,dphy,tstep,tol)),2*N,1);
	
	%pos(:,i+1,1:2)=xtarget;
	%draw(Q(:,i),width,height,rphy);
end
draw(Q,width,height,rphy);
%draw([pos(:,:,1);pos(:,:,2)],width,height,rphy);
toc
