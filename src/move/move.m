function newPos=move(x,xtarget,absv,dphy,tstep,tol)
%
N=size(x,1); newPos=zeros(N,2); 
%
direc=zeros(N,2); v=zeros(N,2);
p=zeros(N,N,2); q=zeros(N,N,2); t=zeros(N,N);
tstop=inf*ones(N,1); 

%
direc=[xtarget(:,1)-x(:,1),xtarget(:,2)-x(:,2)];
% Richtungen, in die sich die Schafe momentan bewegen (nicht normiert).
for j=1:N
	if norm(direc(j,:))>0
		v(j,:)=direc(j,:)/norm(direc(j,:));
	else 
		v(j,:)=direc(j,:);
	end
	v(j,:)=absv(j)*v(j,:);
end

tcurr=0;
for k=1:N 
	q(k,:,1:2)=[x(:,1)-x(k,1),x(:,2)-x(k,2)];
	p(k,:,1:2)=[v(:,1)-v(k,1),v(:,2)-v(k,2)];
	for l=1:N
		t(k,l)=findTS(squeeze(p(k,l,1:2)),squeeze(q(k,l,1:2)),dphy,tcurr,tstep,tol);
	end
	%t(k,k)=inf;
end
%
fillPercentage=findFill(t);
%
[colmin,colindx]=min(t);
[tcurr,n]=min(colmin);
m=colindx(n);
%
iteration=0;
while tcurr<tstep && iteration<=N^2 
	if iteration==N^2
		warning('moveTest: Maximal number of iterations reached.');
	end
	iteration=iteration+1;
	if tstop(m)==inf % Markiere Schaf als nicht bewegend.
		tstop(m)=tcurr; 
	end
	if tstop(n)==inf % Beide Schafe halten gleichzeitig.
		tstop(n)=tcurr; 
	end
	[t,p,q]=stopSheep(t,p,q,tstop,x,v,dphy,m,tstep,tol);
	[t,p,q]=stopSheep(t,p,q,tstop,x,v,dphy,n,tstep,tol);
	[colmin,colindx]=min(t);
	[tcurr,n]=min(colmin);
	m=colindx(n);
	%pause
end

for j=1:N
	newPos(j,:)=x(j,:)+min([tstep,tstop(j)])*v(j,:);
end
%
% Nur zu Testzwecken.
if checkDistances([newPos(:,1),newPos(:,2)],dphy,tol)==0
	warning('moveTest: Minimal distance below physical distance.');
	return;
end
