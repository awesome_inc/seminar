%% Potential DEMO Script
%%
%%

% Lade zugeh�rige Pakete
SETUP

clear all;
close all;

%% Designparameter des Potentials
% Beispiel 1
px1 = [-15 -10 0 10 15];
py1 = [0 0 10 0 0];
% Beispiel 2
px2 = [-15 -10 -5 0 5 10 15];
py2 = [0 0 -2 10 -2 0 0];
% Beispiel 3
px3 = [-25 -20 -15 -5 -2 0 2 5 15 20 25];
py3 = [0 0 -1 -1 0 10 0 -1 -1  0  0];
% Beispiel 4
px4 = [-15 -13 -10 -8 -2 0 2 8 10 13 15];
py4 = [0 0 0 -1.5 -1.5 10 -1.5 -1.5 0 0 0];

%% Wertebereich, �ber welchen das Potential berechnet werden soll
x_eval = [-30 30];
y_eval = [-30 30];


%% Beispiel 1
figure('Name','Potential 1');
subplot(1,2,1);
[x,y] = scalar_potential(x_eval,px1,py1);
plot(x,y);
hold on;
plot(px1,py1,'*r');
hold off;
subplot(1,2,2);
[XX,YY,ZZ] = vector_potential(x_eval,y_eval,px1,py1);
surf(XX,YY,ZZ);

%% Beispiel 2
figure('Name','Potential 2');
subplot(1,2,1);
[x,y] = scalar_potential(x_eval,px2,py2);
plot(x,y);
hold on;
plot(px2,py2,'*r');
hold off;
subplot(1,2,2);
[XX,YY,ZZ] = vector_potential(x_eval,y_eval,px2,py2);
surf(XX,YY,ZZ);

%% Beispiel 3
figure('Name','Potential 3');
subplot(1,2,1);
[x,y] = scalar_potential(x_eval,px3,py3);
plot(x,y);
hold on;
plot(px3,py3,'*r');
hold off;
subplot(1,2,2);
[XX,YY,ZZ] = vector_potential(x_eval,y_eval,px3,py3);
surf(XX,YY,ZZ);

figure('Name','Potential 3 - Force');
N = 200;
Q = 40*rand(N,1) - 20;
QXY = reshape(Q,N/2,2);
P = vector_force(Q,px3,py3);
PXY = reshape(P,N/2,2);
subplot(1,2,1);
contourf(XX,YY,ZZ,[min(min(ZZ)), -2, -1, 0:0.5:2, 3, 4, max(max(ZZ))]);
hold on;
plot(QXY(:,1),QXY(:,2),'k*','LineWidth',0.5);
hold off;
subplot(1,2,2);
quiver(QXY(:,1),QXY(:,2),PXY(:,1),PXY(:,2));

%% Beispiel 4
figure('Name','Potential 4');
subplot(1,2,1);
[x,y] = scalar_potential(x_eval,px4,py4);
plot(x,y);
hold on;
plot(px4,py4,'*r');
hold off;
subplot(1,2,2);
[XX,YY,ZZ] = vector_potential(x_eval,y_eval,px4,py4);
surf(XX,YY,ZZ);
