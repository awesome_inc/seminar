if(~exist('csape'))
	disp('Paket `splines` wird benötigt');
	an = input('Paket installieren? (y/n) : ','s');
	if strcmp(lower(an),'y')
		pkg install -forge splines
		pkg load splines
	else
		error('FEHLER: Paket wird für die Ausührung von `MAIN` beötig! Beende Programm.');
	end
end
