function [X,Y] = scalar_potential(x_eval, px, py)
%SCALAR_POTENTIAL Summary of this function goes here
%   Detailed explanation goes here
    
    if(py(1) ~= 0)
        py = [0 py];
        px = [px(1)-1 px];
    end
    if(py(end) ~= 0)
        py = [py 0];
        px = [px px(end)+1];
    end
    pp = csape(px,py,'variational');
    
    NN = max(max(diff(x_eval)),100);
    X = linspace(x_eval(1),x_eval(end),NN);
    Y = ppval(pp,X);
    Y(abs(X)>max(abs(px))) = 0;
end

