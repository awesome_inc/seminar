%% Potential DEMO Script
%%
%%

% Lade zugeh�rige Pakete
SETUP

clear all;
close all;

%% Designparameter des Potentials
v_ab = 2;
v_an = -1;
rphy = .5;
N = 30;
%px = 1.2*[-25 -22 -18 -15 -11.5 -7 -3 0 3 7 11.5 15 18 22 25];
%py = 1/log(N)*[0 0.3*v_an 0.6*v_an v_an 0.6*v_an 0.3*v_an 0 v_ab 0 0.3*v_an 0.6*v_an v_an 0.6*v_an 0.3*v_an 0];
px = 3*[-10:2.5:-2.5 -2 0 2 2.5:2.5:10];
py = [0 0.5*v_an 0.5*v_an 0 0.2*v_ab v_ab 0.2*v_ab 0 0.5*v_an 0.5*v_an 0];

%% Wertebereich, �ber welchen das Potential berechnet werden soll
x_eval = [-40 40];
y_eval = [-30 30];

%% Berechne Potential
[x,y] = scalar_potential(x_eval,px,py);
[XX,YY,ZZ] = vector_potential(x_eval,y_eval,px,py);

%% Plotte Ergebnis
figure('Name','Potential');
subplot(1,2,1);
plot(x,y);
hold on;
plot(px,py,'*r');
axis([x_eval(1) x_eval(2) -5 5]);
hold off;
subplot(1,2,2);
surf(XX,YY,ZZ);
axis equal;

%% Berechne Kr�fte
N = 200;
Q = 40*rand(N,1) - 20;
QXY = reshape(Q,N/2,2);
P = -1*vector_force(Q,px,py);
PXY = reshape(P,N/2,2);

%% Plotte Ergebnis
figure('Name','Force');
subplot(1,2,1);
contourf(XX,YY,ZZ,[min(min(ZZ)), -2, -1, 0:0.5:2, 3, 4, max(max(ZZ))]);
hold on;
plot(QXY(:,1),QXY(:,2),'k*','LineWidth',0.5);
axis equal;
hold off;
subplot(1,2,2);
LP  = sqrt(PXY(:,1).^2 + PXY(:,2).^2) ;
PXY = -1*PXY ./ [LP LP] * 0.1;
quiver(QXY(:,1),QXY(:,2),PXY(:,1),PXY(:,2));
axis equal;
