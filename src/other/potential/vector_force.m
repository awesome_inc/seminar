function P = vector_force(Q_eval, px, py)
%SCALAR_POTENTIAL Summary of this function goes here
%   Detailed explanation goes here
    
    if(py(1) ~= 0)
        py = [0 py];
        px = [px(1)-1 px];
    end
    if(py(end) ~= 0)
        py = [py 0];
        px = [px px(end)+1];
    end
    
    pp = csape(px,py,'variational');
    dp = fnder(pp);  
    
    N = 0.5*length(Q_eval);
    XX = sqrt(Q_eval(1:N).^2+Q_eval((N+1):end).^2);
    
    ZZ = ppval(dp, XX);
    P = 0*Q_eval;
    P(1:N) = (1./XX).* ZZ .* Q_eval(1:N);
    P((N+1):end) = (1./XX).* ZZ .* Q_eval((N+1):end);

end

