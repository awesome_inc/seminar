function [XX,YY,ZZ] = vector_potential(x_eval, y_eval, px, py)
%SCALAR_POTENTIAL Summary of this function goes here
%   Detailed explanation goes here
    
    if(py(1) ~= 0)
        py = [0 py];
        px = [px(1)-1 px];
    end
    if(py(end) ~= 0)
        py = [py 0];
        px = [px px(end)+1];
    end
    
    pp = csape(px,py,'variational');
    
    NX = max(max(diff(x_eval)),100);
    NY = max(max(diff(y_eval)),100);
    
    
    X = linspace(x_eval(1),x_eval(end),NX);
    Y = linspace(y_eval(1),y_eval(end),NY);
    [XX,YY] = meshgrid(X,Y);
    
    ZZ1 = ppval(pp, sqrt(XX.^2 + YY.^2));

    if(size(ZZ1,1)==1)
	    ZZ = 0*XX;
	    ZZ(:,:) = ZZ1(1,:,:);
    else
	    ZZ = ZZ1;
    end
    
    ZZ(sqrt(XX.^2 + YY.^2) >px(end)) = 0;
end

