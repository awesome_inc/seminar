function animation( Coord, Tr, timelapse, opt, col)
    if nargin<5
        col = 'b';
    end
    if nargin < 4
        opt = 0;
    end
    if nargin < 3
        timelapse = 0;
    end
    
    hold on;
    
    if timelapse
        for i=1:length(Tr)
            Px = [Coord(Tr(i,:),1);Coord(Tr(i,1),1)]';
            Py = [Coord(Tr(i,:),2);Coord(Tr(i,1),2)]';
            plot(Px, Py, [col,'-']);

            pause(0.1)
        end
    else
        for i=1:length(Tr)
            Px = [Coord(Tr(i,:),1);Coord(Tr(i,1),1)]';
            Py = [Coord(Tr(i,:),2);Coord(Tr(i,1),2)]';
            plot(Px, Py, [col,'-']);

        end
    end
    if opt==1
        plot(Coord(:,1),Coord(:,2),'r.');
    end
    hold off;
end

