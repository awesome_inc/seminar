function [Coord,Tr,Tx,dx,dy,nx,ny] = createGeometry(id,size)
       
    switch id
        case 0
            if nargin<2
                size = 2;
            end
            % Standart Geometrie
            [Coord,Tr] = createRectangle(100,100,size);
            Tx = [];
            
            % Erzeuge "unsichtbare" umrandung des Gebiets
            bc1 = [0 0;2*size 100];
            bc2 = [0 100-2*size; 100 100];
            bc3 = [0 0;100 2*size];
            bc4 = [100-2*size 0;100 100];
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc1);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc2);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc3);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc4);
            
            dx = Coord(2,1)-Coord(1,1);
            dy = dx;
            nx = sqrt(length(Coord));
            ny = nx;
        case 1
            if nargin<2
                size = 2;
            end 
            [Coord,Tr] = createRectangle(100,100,size);
            Tx = [];
            
            % Erzeuge "unsichtbare" umrandung des Gebiets
            bc1 = [0 0;2*size 100];
            bc2 = [0 100-2*size; 100 100];
            bc3 = [0 0;100 2*size];
            bc4 = [100-2*size 0;100 100];
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc1);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc2);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc3);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc4);
            
            % Entferne Rechteck im Zentrum, (40,40),(60,60)
            corners = [40 40;60 60];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            dx = Coord(2,1)-Coord(1,1);
            dy = dx;
            nx = sqrt(length(Coord));
            ny = nx;
        case 2
            if nargin<2
                size = 2;
            end
            [Coord,Tr] = createRectangle(200,200,size);
            Tx = [];
            
            % Erzeuge "unsichtbare" umrandung des Gebiets
            bc1 = [0 0;2*size 200];
            bc2 = [0 200-2*size; 200 200];
            bc3 = [0 0;100 2*size];
            bc4 = [200-2*size 0;200 200];
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc1);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc2);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc3);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc4);
            
            % Entferne schmales Rechteck auf der linken Seite(40,40),(50,160)
            corners = [40 40;50 160];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
           
            % Entferne schmales Rechteck auf der rechten Seite(150,40),(160,160)
            corners = [150 40;160 160];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
						dx = Coord(2,1)-Coord(1,1);
						dy = dx;
						nx = sqrt(length(Coord));
						ny = nx;
        case 3
            if nargin<2
                size = 1.5;
            end
            [Coord,Tr] = createRectangle(150,150,size);
            Tx = [];
            
            % Erzeuge "unsichtbare" umrandung des Gebiets
            bc1 = [0 0;2*size 150];
            bc2 = [0 150-2*size; 150 150];
            bc3 = [0 0;150 2*size];
            bc4 = [150-2*size 0;150 150];
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc1);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc2);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc3);
            [Coord,Tr,~] = removeRectangle(Coord,Tr,Tx,bc4);
            
            % Entferne Kasten um das Gehege
            corners = [45 95;50 150];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            corners = [50 95;72 100];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            corners = [78 95;100 100];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            corners = [100 95;105 150];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            dx = Coord(2,1)-Coord(1,1);
            dy = dx;
            nx = sqrt(length(Coord));
            ny = nx;
    end
            
end

