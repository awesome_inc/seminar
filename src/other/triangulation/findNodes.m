function selected = findNodes(Coord, points)
    selected = zeros(size(points,1),1);
    for i=1:size(points,1)
        redy = find(abs(points(i,2)-Coord(:,2)) == min(abs(points(i,2)-Coord(:,2))));
        selected(i) = redy(1) -1 + find(abs(Coord(redy,1)-points(i,1)) == min(abs(Coord(redy,1)-points(i,1))));
    end

end

