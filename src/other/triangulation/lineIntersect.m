% lineIntersect, 
% Version 1.0r1 , 2014-01-26, Dominik Linn
%
% Berechnet Approximation einer Strecke in einem gerasterten oder 
% trianguliertem Gebiet.
%
%INPUT:    Coord   Koordinatenvektor
%           dx, dy  Rastergr��e
%           nx, ny  Anzahl der Rasterpunkte
%           P1, P2  Start- und Endpunkt der Strecke
%(optional) Tr      Triangulation, 
% 
%OUTPUT:    Co      Approximation der Strecke durch Rasterpunkte
%(optional) Trx     Approximation der Strecke durch Dreiecke
%
% 
function varargout = lineIntersect(Coord,dx,dy,nx,ny,P1,P2,Tr)
    % Steigung
    m = (P2(2)-P1(2))./(P2(1)-P1(1));
    M = abs(m * dx/dy);
    
    % finde Knoten, der Starpunkt am n�chsten liegt (nicht
    % vektorisierbar?!?)
    sel_x = abs(Coord(:,1)-P1(:,1)) ==  min(abs(Coord(:,1)-P1(:,1)));
    sel_y = abs(Coord(:,2)-P1(:,2)) ==  min(abs(Coord(:,2)-P1(:,2)));
    
    i = mod(find(sel_x&sel_y >0)-1,nx)+1;
    j = ceil(find(sel_x&sel_y >0)/nx);
    
    sx = sign(P2(1)-P1(1));
    sy = sign(P2(2)-P1(2));
    Nx = 1 + floor(abs(P2(1) - P1(1))/dx);
    Ny = 1 + floor(abs(P2(2) - P1(2))/dy);
    
   
    if M<=1
        Co = i+(0:sx:sx*(Nx-1)) + nx*(j-1 + round(0:M:M*(Nx-1))); 
    else
        Co = nx*(j-1+(0:sy:sy*(Ny-1))) + i + round(0:1/M:(Ny-1)/M);
    end
    
    varargout{1} = Co;
    
    if nargout>1
        assert(nargin == 8, 'Mehr Parameter ben�tigt');
        varargout{2} = Tr(sum(ismember(Tr,Co),2)>0,:);
    end 
end