function [Coord, Tr] = removeDoubles( Coord, Tr )
    
    
    % Finde doppelte Koordinaten
    i = 1;
    while i < size(Coord,1)
       test = Coord(i,:);
       id = i+find(Coord(i+1:end,1) == test(1) & Coord(i+1:end,2) == test(2));
       
       if ~isempty(id)
        for k = id
            Tr(Tr == k) = deal(i);
        end
       
        for k = id
            Tr(Tr > k) = Tr(Tr >k) - 1;
            Coord(k,:) = [];
        end
       end
       i = i+1;
    end
    
    % Finde doppelte Dreiecke
    i=1;
    while i < size(Tr,1)-1
       test = Tr(i,:);
       id = find(Tr((i+1):end,1) == test(1) & Tr((i+1):end,2) == test(2) & Tr((i+1):end,3) == test(3));
       Tr(id,:) = [];
       i = i+1;
    end
    
end

