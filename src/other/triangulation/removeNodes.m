function [Coord, Tr, Tx] = removeNodes(Coord, Tr, Tx,nodes)
    for i=1:length(nodes)
        Tx = [Tx;Tr(Tr(:,1)==nodes(i),:)];
        Tr(Tr(:,1)==nodes(i),:) = [];
        Tx = [Tx;Tr(Tr(:,2)==nodes(i),:)];
        Tr(Tr(:,1)==nodes(i),:) = [];
        Tx = [Tx;Tr(Tr(:,3)==nodes(i),:)];
        Tr(Tr(:,1)==nodes(i),:) = [];
    end
    [Coord,Tx] = removeDoubles(Coord,Tx);
end

