clear all;
close all;

dxy = 1;
lxy = 50;

[Coord,Tr] = createRectangle(lxy,lxy,dxy);

nxy = ceil(lxy/dxy)+1;
ntr = size(Tr,1);
R = 1:0.5:5;
t = zeros(length(R),2);

for r = 1:length(R)
    REP = ceil(10^R(r));
    P1 = 20*rand(REP,2);
    P2 = 20*rand(REP,2);

    tic;
    for i=1:REP
        Co = [];
        Co = lineIntersect(Coord,dxy,dxy,nxy,nxy,P1(i,:),P2(i,:));
    end
    t(r,1) = toc;

    tic;
%     for i=1:REP
%         [Co,Trx] = lineIntersect(Coord,dxy,dxy,nxy,nxy,P1(i,:),P2(i,:),Tr);
%     end
%     t(r,2) = toc;
end

figure('Name','Gesamtzeit');
semilogx(R,t(:,1),'r');
hold on;
semilogx(R,t(:,2),'b--');
legend('ohne Dreieck','mit Dreieck');

% figure('Name','Zeit/Aufruf');
% semilogx(R,t(:,1)./ceil(10.^R)','r');
% hold on;
% semilogx(R,t(:,2)./ceil(10.^R)','b--');
% legend('ohne Dreieck','mit Dreieck');
