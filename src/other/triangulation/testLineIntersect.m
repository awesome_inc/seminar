close all;
clear all;

dxy = 1;
lxy = 20;

[Coord,Tr] = createRectangle(lxy,lxy,dxy);
plot(Coord(:,1),Coord(:,2),'.','Color', [.9 .9 .9]);

nxy = ceil(lxy/dxy)+1;

P1 = [3.8 10.9];
P2 = [5.7 18.3];
Co1 = lineIntersect(Coord,dxy,dxy,nxy,nxy,P1,P2);
Tr1 = Tr(sum(ismember(Tr,Co1),2)>0,:);

Q1 = [2.2 0.8];
Q2 = [15.7 3.8];
Co2 = lineIntersect(Coord,dxy,dxy,nxy,nxy,Q1,Q2);
Tr2 = Tr(sum(ismember(Tr,Co2),2)>0,:);

% R1 = [15.3 5.7];
% R2 = [1.1 8.3];
% Co3 = lineIntersect(Coord,dxy,dxy,nxy,nxy,R1,R2);
% Tr3 = Tr(sum(ismember(Tr,Co3),2)>0,:);


hold on;
animation(Coord,Tr1,0,0,'r');
pause(.5);

hold on;
plot([P1(1) P2(1)],[P1(2) P2(2)],'LineWidth', 2, 'Color', 'red');

hold on;
pause(.5);
plot(Coord(Co1,1),Coord(Co1,2),'k.', 'LineWidth', 2);
pause(.5);

hold on;
animation(Coord,Tr2,0,0,'g');
pause(.5);

hold on;
plot([Q1(1) Q2(1)],[Q1(2) Q2(2)],'LineWidth', 2, 'Color', 'green');
pause(.5);
plot(Coord(Co2,1),Coord(Co2,2),'k.', 'LineWidth', 2);
pause(.5);

% hold on;
% plot([R1(1) R2(1)],[R1(2) R2(2)],'c-');
% pause(.5);
% plot(Coord(Co3,1),Coord(Co3,2),'k*');
% pause(.5);
% animation(Coord,Tr3,0,0,'c');
