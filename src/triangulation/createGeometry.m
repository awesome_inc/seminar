function [Coord,Tr,Tx] = createGeometry(id,size)
       
    switch id
        case 0
            if nargin<2
                size = 2;
            end
            % Standart Geometrie
            [Coord,Tr] = createRectangle(100,100,size);
            Tx = [];
            animation(Coord,Tx,0,1);
        case 1
            if nargin<2
                size = 2;
            end 
            [Coord,Tr] = createRectangle(100,100,size);
            Tx = [];
            % Entferne Rechteck im Zentrum, (40,40),(60,60)
            corners = [40 40;60 60];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            animation(Coord,Tx,0,1);
        case 2
            if nargin<2
                size = 2;
            end
            [Coord,Tr] = createRectangle(200,200,size);
            Tx = [];
            % Entferne schmales Rechteck auf der linken Seite(40,40),(50,160)
            corners = [40 40;50 160];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
           
            % Entferne schmales Rechteck auf der rechten Seite(150,40),(160,160)
            corners = [150 40;160 160];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            animation(Coord,Tx,0,1);
        case 3
            if nargin<2
                size = 1.5;
            end
            [Coord,Tr] = createRectangle(150,150,size);
            Tx = [];
            % Entferne Kasten um das Gehege
            corners = [45 95;50 150];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            corners = [50 95;72 100];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            corners = [78 95;100 100];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
            corners = [100 95;105 150];
            [Coord,Tr,Tx] = removeRectangle(Coord,Tr,Tx,corners);
%%            animation(Coord,Tx,0,1);
    end
            
end

