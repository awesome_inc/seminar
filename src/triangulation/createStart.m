%% ---- createStart.m v. 0.2.0 07.01.2014 (Ehmann, Hans, Kluender, Linn, Ye) ----
%%
%% [Q,Tx,Ty]=createStart(N,                         % Anzahl der Schafe
%%                       Co,                        % Koord. der Triangulierung
%%                       Tr,                        % Triang. (begehbare Flaeche)
%%                       rho (optional, default: 5) % Wohlfuehldichte [m^2/Schaf]
%%                      )
%% Q      = Startposition der Schafe (2*N x 1)
%% Tx, Ty = (optional) Startflaeche (fuer draw.m)
%%
%% !!! SCHNELLER TEST !!!:
%% [Co,Tr]=test; [Q,Tx,Ty]=createStart(N=50,Co,Tr); draw(Q,Co,Tr,Tx,Ty);
%% ------------------------------------------------------------------------------
function [Q,Tx,Ty]=createStart(N,Co,Tr,rho)        % Tx, Ty optional zu Testzwecken
tic

%%% --- setze defaut-Werte fuer undefinierte Variablen ------------------------------------------------
%%% ---------------------------------------------------------------------------------------------------
if exist('rho','var') == 0  ; rho   = 5 ; end      % Dichte: hier 5 m^2/Schaf


%%% --- Definition weiterer Variablen -----------------------------------------------------------------
%%% ---------------------------------------------------------------------------------------------------
Cx    = Co(:,1)             ;                      % x-Koordinaten der Ecken
Cy    = Co(:,2)             ;                      % y-Koordinaten der Ecken
Tx    = Cx(Tr')             ;                      % x- und y-Koordinaten der einzelnen Triangulierungs-
Ty    = Cy(Tr')             ;                      % dreiecke der begehbaren Flaeche
A_max = sum(polyarea(Tx,Ty));                      % Flaecheninhalt der begehbaren Flaeche
A     = N*rho               ;                      % benoetigte Flaeche der N Schafe


%%% --- Auwahl von zusammenhaengenden Dreiecken aus der Triangulierung Tr, mit Flaecheninhalt ---------
%%% --- mindestens A + eps.; Ausgabe Tx, Ty (wie Tr), fuer x- und y-Koordinaten der Dreicke -----------
a = 0;                                             % 'aktueller' Flaecheninhalt in der Schleife
if A_max > A                                       % verwende sonst komplette Flaeche Tx und Ty
  e=Tr(ceil(length(Tr)*rand),1);                   % waehle zufaelligen 'ersten' Eckpunkt in Tr
  E=Tr(any(Tr'==e),:);                             % E enthaelt alle Dreicke, die e enthalten (wie Tr)
                                                   % und die im 'alten' E enthalten waren
  while a <= A
    e=E(ceil(length(E)*rand),round(rand*2)+1);     % waehle zufaelligen Eckpunkt aus E
    E=unique([E;Tr(any(Tr'==e),:)],'rows');        % loesche (unique) doppelte Zeilen in E

    Tx=Cx(E'); Ty=Cy(E'); a=sum(polyarea(Tx,Ty));  % hier wird die Flaeche a neu berechnet
  end

end


%%% --- Verteile die Schafe auf die Ecken der durch Tx und Ty gegebenen Teilflaeche, so dass ----------
%%% --- keine Ecke doppelt besetzt ist. Ueberlappung der Schafe moeglich bei zu hoher Aufloesung. -----
Q=[];
while size(Q,1) < N
  Mx = ceil(rand*size(Tx,1));                      % zufaellige x- und y- Koordinate aus der, durch
  My = ceil(rand*size(Tx,2));                      % Tx und Ty vorgegebenen Flaeche
  Q = [Q; Tx(Mx,My), Ty(Mx,My)];                   % Q (hier) hat das Format [x_1,y_1;x_2,y_2; ...]
  Q = unique([Q; Tx(Mx,My), Ty(Mx,My)],'rows');    % Loesche von Doppeltbesetzungen
end
Q=Q(:);                                            % Formataenderung fuer Q [x_1;x_2;...;y_1;y_2;...]

toc
%%% --- es kommt zu hohen Rechenzeiten, wenn ...
%%% - Die Fläche A sehr groß ist, aber kleiner als A_max (Schafanzahl nahe bei A/rho)
%%% - es mehr Schafe, als begehbare Eckpunkte gibt (Endlosschleife)
