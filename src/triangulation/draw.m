%% ---- draw.m v. 0.4.2 07.01.2014 (Ehmann, Hans, Kluender, Linn, Ye) ------------
%%
%% ---- !!! ACHTUNG VERAENDERTE SYNTAX GEGENUEBER v. 0.4.0 !!!
%% draw(Q,                                    Matrix:        <2*N x T>
%%      Co,                                   Ecken:         <#Ecken x 2>
%%      Tr (nur zu Testzwecken),              Kanten:        <#KFeld x 3>
%%      Tx (aus createStart; Test),           x-Koordinaten Startflaeche
%%      Ty (aus createStart; Test),           y-Koordinaten Startflaeche
%%      H  (momentan auskommentiert),         Hindernisse:   <#KHind x 3>
%%      r_phy,   (optional, default: .5   )   phy. Radius:   <positive Zahl>
%%      trajek,  (optional, default: false)   Trajektorie:   <Vektor Schafnummern>
%%      plotnr,  (optional, default: false)   nummerieren:   <true/false>
%%      sheeps,  (optional, default: [1:N])   Schafe:        <Vektor Schafnummern>
%%      delay,   (optional, default: .1   )   Pause:         <positive Zahl>
%%      savpng,  (optional, default: false)   speichere png: <true/false>
%%      nangle   (optional, default: 10   )   Eckenzahl:     <positve ganze Zahl>
%%     )
%% -------------------------------------------------------------------------------
function [] = draw(Q, Co, H, r_phy, trajek, plotnr, sheeps, delay, savpng, nangle, Tx, Ty)
tic

%%% --- setze default-Werte fuer undefinierte Variablen -----------------------------------------------
%%% ---------------------------------------------------------------------------------------------------
N  = size(Q,1)/2             ;                     % Anzahl der Schafe                | default-Wert:
T  = size(Q,2)               ;                     % Anzahl der Zeitschritte          + ---------------
if exist('r_phy' ,'var') == 0; r_phy  = .47   ; end % physischer Radius des Schafes    | 0.5
if exist('trajek','var') == 0; trajek = 0    ; end % Trajektorie zeichnen fuer Schafe | abgeschaltet
if exist('plotnr','var') == 0; plotnr = 0    ; end % Schafnummer plotten              | abgeschaltet
if exist('sheeps','var') == 0; sheeps = [1:N]; end % Schafe, die angezeigt werden     | alle
if exist('delay' ,'var') == 0; delay  = .1   ; end % Anzeigedauer fuer Frames         | 0.1 Sekunden
if exist('savpng','var') == 0; savpng = 0    ; end % speichere Frames in ./plot/*.png | abgeschaltet
if exist('nangle','var') == 0; nangle = 10   ; end % Anzahl der Ecken des Polygons    | 10
t_circ = [0:1/nangle:1]*2*pi ;                     % Laufvariable Eckpunkte des Polygons
L      = length(sheeps)      ;                     % zu plottende Anzahl von Schafen
width  = max(Co(:,1))        ;                     % Laenge des Feldes
height = max(Co(:,2))        ;                     % Hoehe des Feldes


%%% --- erstelle die zu plottenden Daten [ (nangle + 1)x(N*T)-Matrix ] --------------------------------
%%% ---------------------------------------------------------------------------------------------------
first=Q(sheeps, 1:T); second=Q(N + sheeps,1:T);
plotxm=r_phy.*sin(t_circ')*ones(1,L*T) + ones(nangle+1,L*T)*diag(first(:));
plotym=r_phy.*cos(t_circ')*ones(1,L*T) + ones(nangle+1,L*T)*diag(second(:));

% for i = 1:size(Tr,1);                              % Feld (kann spaeter weggelassen werden)
%   x(i,:)=Co(Tr(i,:),1);                            % Die for-Schleifen kann man sicher vektorisieren
%   y(i,:)=Co(Tr(i,:),2);                            % ich denke bei Gelegenheit darueber nach
% end

for i = 1:size(H,1);                             % Hindernisse
  X(i,:)=Co(H(i,:),1);
  Y(i,:)=Co(H(i,:),2);
end


%%% --- beginne mit dem Plot --------------------------------------------------------------------------
%%% ---------------------------------------------------------------------------------------------------

hold on;

for i = 1:T
  cla;               %% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  k=[(i-1)*L+1:i*L]; %% !!! Das ist sehr unschoen, dass Hindernisse immer wieder neu geplottet werden!
                     %% !!! aber wegen cla ist das noetig (HAT JEMAND EINE IDEE, WIE MAN DAS UMGEHEN
                     %% !!! KANN? - WIRKT SICH SEHR DEUTLICH AUF DIE LAUFZEIT AUS!!!)
                     %% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%  patch([0,width,width,0],[0,0,height,height],'r');
%  patch(x',y','w');  %% <-- DOOF - SOLLTE VOR      % plotte das Bewegungsfeld
  patch(X',Y','r');  %% <-- SCHLEIFE ERLEDIGT WERDEN % plotte die Hindernisse
  axis('equal', [0, width, 0, height]); %% <--     % in Matlab koennen die Axen einmalig gesetzt werden
  text(0,26/25*height,sprintf('Zeitschritt: %.0f/%g',i,T)); % aktuellen Zeitschritt im Plot anzeigen

  if trajek ~= 0                                   % zeichne die Trajektorie(n)
    tx=[Q(trajek  ,1:i),NaN(length(trajek),1)]';
    ty=[Q(N+trajek,1:i),NaN(length(trajek),1)]';
    plot(tx,ty,'-','Color', [.85 .85 .85]);
  end
  if exist('Tx','var') == 1 && exist('Ty','var') == 1
    patch(Tx,Ty,'g');
  end
  patch(plotxm(:,k),plotym(:,k),'b');              % Kreisscheibe um alle Schafe zeichnen

  if plotnr == 1                                   % nummeriere die Schafe
    for j = sheeps
      text(Q(j,i)+1.5,Q(N+j,i)-.5,sprintf('%.0f',j));
    end
  end

  if savpng == 1                                   % benoetigt: epstool; fig2dev (package transfig);
    filename=sprintf('plot/%05d.png',i);           %            pstoedit
    print(filename);
  end

  pause(delay);
end
hold off;
toc
